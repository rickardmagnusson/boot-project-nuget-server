﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace $rootnamespace$.Models
{
    public class BootModel
    {
        public virtual Int32 Id { get; set; }
        public virtual string Text { get; set; }
    }

    public class BootModelMap : ClassMap<BootModel>
    {
        public BootModelMap()
        {
            Id(p => p.Id)
                .GeneratedBy
                  .Assigned()
                    .Not.Nullable();
            Map(p => p.Text);
        }
    }
}