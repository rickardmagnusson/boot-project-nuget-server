﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Boot.Multitenancy;
using Boot.Multitenancy.Extensions;
using $rootnamespace$.ViewModels;

namespace $rootnamespace$.Controllers
{
    public class BootController : Controller
    {
        BootViewModel Model = new BootViewModel();

        public ActionResult Index()
        {
            return View(Model);
        }
	}
}