﻿using System;
using System.IO;
using System.Linq;
using Boot.Multitenancy;
using Boot.Multitenancy.Extensions;
using FluentNHibernate.Conventions;
using $rootnamespace$.Models;

namespace $rootnamespace$.Environment
{
    public static class Host
    {
        public static void Init()
        {
            //Create 2 test databases.
            SessionFactoryContainer.Current
                .Add("First", new Tenant("Data Source=|DataDirectory|FirstDB.sdf;Persist Security Info=False;").Create())
                .Add("Second", new Tenant("Data Source=|DataDirectory|SecondDB.sdf;Persist Security Info=False;").Create());

            CheckEnvironmentSetup();
        }

        private static void CheckEnvironmentSetup()
        {
            if (SessionFactory.With<BootModel>().IsNotAny()) { 

                SessionFactory
                    .With<BootModel>("First") //Open connection first
                    .OpenSession()
                    //Create 3 new ModelTest items in db...
                    .Save<BootModel>(new BootModel
                    {
                        Id = 1,
                        Text = "First Item.)"
                    })
                    .Save<BootModel>(new BootModel
                    {
                        Id = 2,
                        Text = "BootProject Footer"
                    })
                    .Save<BootModel>(new BootModel
                    {
                        Id = 3,
                        Text = "Welcome to BootProject."
                    });

                SessionFactory
                    .With<BootModel>("Second") //Open connection second
                    .OpenSession()
                    .Save<BootModel>(new BootModel
                    {
                        Id = 1,
                        Text = "This is the result from Second db."
                    });
            }

        }
    }
}