﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Boot.Multitenancy;
using Boot.Multitenancy.Extensions;
using $rootnamespace$.Models;

namespace $rootnamespace$.ViewModels
{
    public class BootViewModel
    {
        public string ResultFromFirstDb { get; set; }
        public string ResultFromSecondDb { get; set; }
        public string FooterText { get; set; }

        public List<BootModel> AllInModelTestOne { get; set; }
        public BootModel ModelTestTwo { get; set; }

        public BootViewModel()
        {
            AllInModelTestOne = SessionFactory
                 .With<BootModel>("First") //Open connection First
                     .OpenSession()
                        .All<BootModel>();

            ModelTestTwo = SessionFactory
                .With<BootModel>("Second") //Open connection Second
                    .OpenSession()
                       .Find<BootModel>(m => m.Id == 1)
                            .FirstOrDefault();

            //...

            ResultFromFirstDb = AllInModelTestOne.Find(t => t.Id == 3).Text;
            ResultFromSecondDb = ModelTestTwo.Text;
            FooterText = AllInModelTestOne.Find(t=> t.Id == 2).Text;
        }
    }
}