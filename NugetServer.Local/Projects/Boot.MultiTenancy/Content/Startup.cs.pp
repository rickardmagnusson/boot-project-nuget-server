﻿using Microsoft.Owin;
using Owin;
using $rootnamespace$.Environment;

namespace $rootnamespace$
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Host.Init();
        }
    }
}